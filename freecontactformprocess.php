<?php
session_start();
require_once 'dbconnect.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
$db = new DB();
if(!empty($_POST['is_quote'])){
	$bore_dia = $_POST['Borewell_Diameter'];
	$yield  = $_POST['Water_Yield'];
	$contact_no  = $_POST['Contact_Number'];
	$email_address  = $_POST['Email_Address'];
	$site  = $_POST['Site_Address'];
	$sql="INSERT INTO `quotes`(`name`,`email`,`contact`,`site`,`bore_dia`,`water_yield`)
	VALUES ('Quotation Request','$email_address','$contact_no','$site','$bore_dia','$yield')";
	$result= $db->executeQuery($sql);
	if($result == TRUE){
		$mail = new PHPMailer;
		// $mail->isSMTP();
		$mail->Host = 'hachiweb.com';
		$mail->Port = 587;
		$mail->SMTPOptions = array(
			'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
			)
		);
		$mail->SMTPAuth = true;
		$mail->Username = 'care@hachiweb.com';
		$mail->Password = 'Vw4&2tw4';
		$mail->setFrom('care@hachiweb.com', 'Shiva Tubewells');
		$mail->addAddress('infoshivatubewells@gmail.com');
		// Add cc or bcc 
		$mail->addCC('info@shivatubewells.com');
		$mail->addBCC('prateek.asthana@hachiweb.com');
		$mail->Subject = 'Shiva Tubewells Quotation';
		$mail->isHTML(true);
		$mailContent = '<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        .main {
            background-color: #6f42c1;
            border-radius: 8px;
        }

        .main span {
            color: white;
            font-size: 20px;
        }

        .main img {
            width: 6%;
            padding: 10px;
            border-radius: 50%;
        }

        body {
            background-color: #f8f9fa;
        }

        .main-body {
            background-color: white;
            border-radius: 8px;
        }
    </style>
</head>

<body>
    <section class="container mt-4">
        <div class="row">
            <div class="col-sm-12">
                <div class="main">
                    <img src="https://www.shivatubewells.com/images/site-logo.png" alt="">
                    <span>Shiva Tubewells</span>
                </div>
            </div>
        </div>
    </section>
    <section class="container mt-4">
        <div class="row">
            <div class="col-sm-12">
                <div class="my-3 p-3 bg-white rounded box-shadow">
                    <h6 class="pb-2 mb-0">Shiva Tubewells Quote</h6>
                    <p class="border-bottom border-gray pb-2 ml-4 mb-0">Quotation</p>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Email- </strong> '.$email_address. '
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Contact- </strong>'.$contact_no.'
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Site- </strong> '.$site.'
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Bore Dia- </strong>'.$bore_dia.' inch
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>';
		$mail->Body = $mailContent;
		if (!$mail->send()) {
			$response['status'] = 400;
			$response['message'] = $mail->ErrorInfo;
			header('Content-Type: application/json');
			header("HTTP/1.1 400 Bad request");
			echo json_encode($response);
			exit();
		}
		header("Location: thankyou.php");
	}
}

$name = $_POST['Full_Name'];
$email  = $_POST['Email_Address'];
$phone  = $_POST['Telephone_Number'];
$message  = $_POST['Your_Message'];
$sql="INSERT INTO `contact`(`name`,`email`,`contact`,`message`)
VALUES ('$name','$email','$phone','$message')";
$result= $db->executeQuery($sql);

if(isset($_POST['Email_Address'])) {
	
	include 'freecontactformsettings.php';
	
	function died($error) {
		echo "Sorry, but there were error(s) found with the form you submitted. ";
		echo "These errors appear below.<br /><br />";
		echo $error."<br /><br />";
		echo "Please go back and fix these errors.<br /><br />";
		die();
	}
	$_POST['AntiSpam'] = 25;
	if(!isset($_POST['Full_Name']) ||
		!isset($_POST['Email_Address']) ||
		!isset($_POST['Telephone_Number']) ||
		!isset($_POST['Your_Message']) || 
		!isset($_POST['AntiSpam'])		
		) {
		died('Sorry, there appears to be a problem with your form submission.');		
	}
	
	$full_name = $_POST['Full_Name']; // required
	$email_from = $_POST['Email_Address']; // required
	$telephone = $_POST['Telephone_Number']; // not required
	$comments = $_POST['Your_Message']; // required
	$antispam = $_POST['AntiSpam']; // required
	
	$error_message = "";
	
	$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(preg_match($email_exp,$email_from)==0) {
  	$error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
  if(strlen($full_name) < 2) {
  	$error_message .= 'Your Name does not appear to be valid.<br />';
  }
  if(strlen($comments) < 2) {
  	$error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }
  
  if($antispam <> $antispam_answer) {
	$error_message .= 'The Anti-Spam answer you entered is not correct.<br />';
  }
  
  if(strlen($error_message) > 0) {
  	died($error_message);
  }
	$email_message = "Form details below.\r\n";
	
	function clean_string($string) {
	  $bad = array("content-type","bcc:","to:","cc:");
	  return str_replace($bad,"",$string);
	}
	
	$email_message .= "Full Name: ".clean_string($full_name)."\r\n";
	$email_message .= "Email: ".clean_string($email_from)."\r\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\r\n";
	$email_message .= "Message: ".clean_string($comments)."\r\n";

	$mail = new PHPMailer;
	// $mail->isSMTP();
	$mail->Host = 'hachiweb.com';
	$mail->Port = 587;
	$mail->SMTPOptions = array(
		'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
		)
	);
	$mail->SMTPAuth = true;
	$mail->Username = 'care@hachiweb.com';
	$mail->Password = 'Vw4&2tw4';
	$mail->setFrom('care@hachiweb.com', 'Shiva Tubewells');
	$mail->addAddress('infoshivatubewells@gmail.com');
	// Add cc or bcc 
	$mail->addCC('info@shivatubewells.com');
	$mail->addBCC('prateek.asthana@hachiweb.com');
	$mail->Subject = 'Shiva Tubewells Contact us';
	$mail->isHTML(true);
	$mailContent = '<!DOCTYPE html>
<html lang="en">

<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        .main {
            background-color: #6f42c1;
            border-radius: 8px;
        }

        .main span {
            color: white;
            font-size: 20px;
        }

        .main img {
            width: 6%;
            padding: 10px;
            border-radius: 50%;
        }

        body {
            background-color: #f8f9fa;
        }

        .main-body {
            background-color: white;
            border-radius: 8px;
        }
    </style>
</head>

<body>
    <section class="container mt-4">
        <div class="row">
            <div class="col-sm-12">
                <div class="main">
                    <img src="https://www.shivatubewells.com/images/site-logo.png" alt="">
                    <span>Shiva Tubewells</span>
                </div>
            </div>
        </div>
    </section>
    <section class="container mt-4">
        <div class="row">
            <div class="col-sm-12">
                <div class="my-3 p-3 bg-white rounded box-shadow">
                    <h6 class="pb-2 mb-0">Shiva Tubewells Contact</h6>
					<p class="border-bottom border-gray pb-2 ml-4 mb-0">Contact</p>
					<div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Name- </strong> '.$full_name. '
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Email- </strong> '.$email_from. '
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Contact- </strong>'.$telephone.'
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Message- </strong> '.$comments.'
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

</html>';
		$mail->Body = $mailContent;
	if (!$mail->send()) {
		$response['status'] = 400;
		$response['message'] = $mail->ErrorInfo;
		header('Content-Type: application/json');
		header("HTTP/1.1 400 Bad request");
		echo json_encode($response);
		exit();
	}
	
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
mail($email_to, $email_subject, $email_message, $headers);
header("Location: $thankyou");
?>
<script>location.replace('<?php echo $thankyou;?>')</script>
<?php
}
die();
?>
