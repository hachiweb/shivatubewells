<?php
  error_reporting(0);
  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
  $link = "https://"; 
  else
  $link = "http://"; 
  $site_url = $link.$_SERVER['HTTP_HOST'];
  $currenturl = $_SERVER['REQUEST_URI'];
  $urlfind = '?'; 
  if (strpos($currenturl, $urlfind) == false) { 
    $currenturl = $_SERVER['REQUEST_URI'];
  } 
  else { 
    $currenturl = substr($currenturl, 0, strpos($currenturl, "?"));
  } 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Shiva Tubewells</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Call +91 8869081529 - Shiva Tubewells, Jhajra, Near Balaji Temple, Dehradun">
    <meta name="keywords" content="borewell dehradun,odex drilling dehradun,water well drilling dehradun,submersible pump dehradun,5, 7, 8 inch ODEX drilling,tubewell contractor Dehradun,borewell contractor Uttarakhand">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/slick.min.css">  
    <link rel="stylesheet" href="css/slick-theme.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/custom.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custom.css" type="text/css">
    <link rel="stylesheet" href="css/custom.scss" type="text/css">
    <script type="text/javascript">//<![CDATA[
      var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
      document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
      //]]>
    </script>
</head>

<body>
  <header class="header-set">
  <div class="main-header">
        <nav class="navbar-expand-md navbar-light navbar-default" id="navbar" role="navigation">
        <a id="logo" href="index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">
                <img src="images/site-logo.png" alt="shiva tubewells logo">
            </a>
            <button class="navbar-toggler pull-right bg-white navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" onclick="closeNav()">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <!-- Links -->
                <ul class="navbar-nav menu-txt ml-auto">
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="home") { echo "active1"; } ?>" href="index<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="about") { echo "active1"; } ?>" href="about-us<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="service") { echo "active1"; } ?>" href="services<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="portfolio") { echo "active1"; } ?>" href="portfolio<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="testimonial") { echo "active1"; } ?>" href="testimonial-detal<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Testimonial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="contact") { echo "active1"; } ?>" href="contact-us<?php echo $site_url=='http://localhost' || $site_url=='https://localhost'?'.php':''; ?>">Contact us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($page=="career") { echo "active1"; } ?>" href="#">Career</a>
                    </li>
                    <li class="nav-item">
                        <a class="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

 <div class="carousel-caption">
      <h4>A TEAM YOU CAN TRUST</h4>
      <h1>UNDERGROUND SPECIALISTS</h1>
      <p>Serving the state for last 15 years</p>
      <div class="mt-5">
          <button type="button" class="btn btn-lg txt-btn">
              <a href="services.php" class="text-white text-decoration-none">VIEW SERVICES</a>
          </button>
          <button type="button" class="btn btn-lg txt-btn" data-toggle="modal" data-target="#exampleModalCenter">
              GET A FREE QUOTE
          </button>
      </div>
  </div>

    <div class="video-container">
    <div id="video_overlays"></div>
      <video autoplay loop muted id="video-bg">
        <source src="video/stw_bg.mp4" type="video/mp4">
      </video>
    </div>

</header>
    
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header bg-color-header">
        <h5 class="modal-title" id="exampleModalLongTitle">FREE QUOTE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="freecontactformprocess.php" method="post">
          <input type="hidden" name="is_quote" value="<?= bin2hex(random_bytes(20));?>" />
         <div class="form-group">
            <label for="Borewell_Diameter" class="col-form-label">Borewell Diameter:</label>
            <input type="number" class="form-control" id="Borewell_Diameter" name="Borewell_Diameter" placeholder="Enter in Inch" required="required">
          </div>
          <div class="form-group">
            <label for="Water_Yield" class="col-form-label">Water Yield:</label>
            <input type="number" class="form-control" id="Water_Yield" name="Water_Yield" placeholder="Enter in Inch" >
          </div>
          <div class="form-group">
            <label for="Contact_Number" class="col-form-label">Contact Number:</label>
            <input type="text" class="form-control Contact_Number" id="Contact_Number" name="Contact_Number" placeholder="Enter Mobile Number" required="required">
          </div>
          <div class="form-group">
            <label for="Address" class="col-form-label">Email Address:</label>
            <input type="email" class="form-control" id="Email_Address" name="Email_Address" placeholder="Enter Email Address" required="required">
          </div>
          <div class="form-group">
            <label for="Site_Address" class="col-form-label">Site Address:</label>
            <textarea class="form-control" id="Site_Address" name="Site_Address" row=""></textarea>
          </div>
      </div>
      <div class="text-center py-3">
        <button type="submit" class="btn btn-custom"><i class="fas fa-envelope mr-2"></i>Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){       
   var scroll_start = 0;
   var startchange = $('#startchange');
   var offset = startchange.offset();
    if (startchange.length){
   $(document).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start > offset.top) {
          $(".navbar-default").css('background-color', '#071529');
          $("ul>li>a").css('color', '#ffffff');
       } else {
          $('.navbar-default').css('background-color', '');
          $("ul>li>a").css('color', '#ffffff');
       }
   });
    }
});
</script>

<!-- When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size -->
<script>
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.padding = "5px 4px";
    document.getElementById("logo").style.fontSize = "10px";
  } else {
    document.getElementById("navbar").style.padding = "12px 5px";
    document.getElementById("logo").style.fontSize = "16px";
  }
}
</script>
