-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2019 at 10:56 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stw`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `contact`, `message`) VALUES
(1, 'Holmes Lowery', 'xoheb@mailinator.net', 9865321475, 'Aut est quis doloribus libero optio ut illum impedit soluta id nostrud voluptates occaecat maiores'),
(2, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed'),
(3, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed'),
(4, 'name', 'sfytgfd@gmail.com', 9852369874, 'site'),
(5, 'ContactForm', 'raphaesiftfriest@gmail.com', 375627528, 'Ciao!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial proposal through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our software and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This technique improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com'),
(6, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Look at a bonzer  wanting to thrive approach as a ripen to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0P2k'),
(7, 'mohan', 'mohan@gmail.com', 7814016006, 'test message\n'),
(8, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'There is a tangibles  value purchase from as a medication seeking your team. shivatubewells.com \r\nhttp://bit.ly/2NJwS2d'),
(9, 'JimmieJax', 'spv529163@gmail.com', 354476655, 'Opportunism in business expansion FINANCIAL capital for NGOs entrepreneurship.FDI seeks to invest through equity, loans or guarantees, and through allocations of public funds to specific projects.'),
(10, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Take notice is  an important  visuals as a recondition to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0vAE'),
(11, 'EddieAmock', 'support@monkeydigital.co', 117343455, 'What will we do to increase your DA? \r\n \r\nOur Plan is very simple yet very effective. We have researched all available sources and gained access to thousands of High DA domains that are directly indexed and re-crawled by Moz Robots on each update. So, we will build and index: \r\n \r\n- 5000 DA30+ backlinks \r\n- 2500 dofollow from high PA pages \r\n- 1000 unique domains links with high DA scores \r\n \r\nstart boosting ranks and SEO metrics with our plan today: \r\nhttp://monkeydigital.co/product/moz-da-seo-plan/ \r\n \r\nBest regards \r\nMike \r\nmonkeydigital.co@gmail.com'),
(12, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Pacify note an astounding  value pass on instead of you. shivatubewells.com \r\nhttp://bit.ly/2NUV7L4'),
(13, 'Williamdet', 'raphaesiftfriest@gmail.com', 277241778, 'Hi!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Contact form are filled in by our application and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161'),
(14, 'YourBusinessFundingNow', 'noreply@your-business-funding-now.info', 89146411334, 'Hi, letting you know that http://Your-Business-Funding-Now.info can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nHave a great day, \r\nThe Your Business Funding Now Team \r\n \r\nunsubscribe/remove - http://your-business-funding-now.info/r.php?url=shivatubewells.com&id=e164'),
(15, 'XaPEjVbDwGzv', 'ab2077230@gmail.com', 3943799398, ''),
(16, 'Michaelwen', 'mircgyhaelBus@gmail.com', 84638323512, 'Look at unblended  inspiriting in rations of victory. shivatubewells.com  http://anuphepma.gq/gez6'),
(17, 'HarryNUATE', 'edwardm@lioncourtcrypto.net', 81735995832, 'We buy all crypto currencies at good rate, with reasonable commission between sellers and the mandates. \r\n \r\nContact us with the informations below if you are or have a potential seller. \r\n \r\nTel: +353 1 4378345 \r\nFax: +353 1 6865354 \r\nEmail: edwardm@lioncourtcrypto.com \r\n \r\nThank you, \r\nCrypto currencies Purchase Department, \r\nEdward Molina.'),
(18, 'Holliscessy', 'contacts@businesswebconnection.com', 85759784251, 'Are you using the internet to its greatest potential?  We are professional web developers that can help you get the most benefit from the web and make your site interactive with your users and clients, making your business more efficient and powerful than your competition. \r\nIf you are just looking to upgrade your web site to current technology or give your business a boost with data integration, we can guide you to success. \r\nhttps://www.businesswebconnection.com/ \r\ncontacts@businesswebconnection.com'),
(19, 'http://v.ht/GaZiZA', 'forever_ever212@hotmail.com', 84488829599, 'http://go-4.net/fn5u'),
(20, 'Thomasdat', 'noreplymonkeydigital@gmail.com', 89821934961, 'Having a better Alexa for your website will increase sales and visibility \r\n \r\nOur service is intended to improve the Global Alexa traffic rank of a website. It usually takes seven days to see the primary change and one month to achieve your desired three-month average Alexa Rank. The three-month average Alexa traffic rank is the one that Alexa.com shows on the Alexa’s toolbar. \r\n \r\nFor more information visit our website \r\nhttps://monkeydigital.co/product/alexa-rank-service/ \r\n \r\nthanks and regards \r\nMike \r\nmonkeydigital.co@gmail.com'),
(21, 'Steviesnoma', 'guqing@chinagiftware.com', 89295984671, 'Wholesale Christmas Decorative Hanging  Ornaments. Online store: http://chinagiftware.com \r\nReindeer Shatterproof Ornament gold or silver Foil Finish + Glitter Finish, \r\nUnicorn Shatterproof Ornament gold or silver Foil Finish + Glitter Finish \r\n \r\nHandmade oil paintings reproductions, Museum quality oil painting reproductions for artist Pino Daeni, Henry Asencio and all Top Artists. A Custom Portrait painting From Photo Online store  http://www.canvaspainting.net/ \r\nWhatsApp: +86-13328758688'),
(22, 'Sandrapsype', '', 85578192798, ' Hy there,  Look what we acquire in place of you! a kinddonation  http://esindiser.tk/3v2f');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `value` text NOT NULL,
  `source` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `type`, `value`, `source`) VALUES
(1, 0, 'We are renowned bore-well developer in the state. We deal with 5\", 7\" & 8\" Odex drilling. We employ ODEX Percussion Down-The-Hole Hammer drilling method. It is an adaptation of the air-operated down-the-hole hammer. It uses a swing-out eccentric bit to ream the bottom of the casing. The percussion bit is a two-piece bit consisting of a concentric pilot bit behind which is an eccentric second bit that swings out to enlarge the hole diameter. Immediately above the eccentric bit is a drive sub that engages a special internal shouldered drive shoe on the bottom of the ODEX casing. The ODEX is thus pulled down by the drill stem as the hole is advanced. Cuttings blow up through the drive sub and casing annuls to a swivel conducting them to a sample collector or onto the ground.\r\n\r\nBesides drilling, we are also authorized distributor of renowned submersible pumps in the state.We have been providing drilling services in the state for last 11 year. Services offered to commercial/non commercial and government projects. We have constructed at least one borewell in every part of the state.\r\nOur team of highly experienced drillers and skilled workers discharge their duties with the aim to deliver satisfactory result. For any assistance or query regarding the bore-well drilling feel free to contact us any time.\r\nTo contact us please navigate to our contact page\r\n\r\nshiva tubewells dehradun\r\nWe own two DTH hydraulic drill with accessories truck mounted for large dia LG compressor with capacity 1000/275 cfm/psi & 900/200 cfm/psi respectively.\r\n\r\n\r\nPlease note that we also hire other rig machines in case of over booking.', 'Home'),
(2, 1, 'http://shivatubewells.com/images/slide3.jpg', 'Home'),
(3, 1, 'http://shivatubewells.com/images/slide6.jpg', 'Home'),
(4, 1, 'http://shivatubewells.com/images/slide4.jpg', 'Home'),
(5, 1, 'http://shivatubewells.com/images/slide1.jpg', 'Home'),
(6, 1, 'http://shivatubewells.com/images/slide5.jpg', 'Home'),
(7, 1, 'http://shivatubewells.com/images/slide2.jpg', 'Home'),
(8, 1, 'http://shivatubewells.com/images/banner1.jpg', 'Home'),
(9, 1, 'http://shivatubewells.com/images/button.jpg', 'Home'),
(10, 0, 'We are Dehradun based govt. regd. leading water drilling firm. We are announced officially in the year 2004 but we\'ve been working much earlier.\r\nWe hold vast drilling experience. We have expertise in 5, 7, & 8\" Odex drilling.\r\nOur targeted customer belongs to the following categories:-\r\nFarmers-Agriculture\r\nIndustrialists-Plants and Machinery industry\r\nBuilders-Real estate (building construction)\r\nGovernment- Civil construction (like bridge, flyover, road, drainage construction or hand pump installations)\r\nCultivation- Nursery, farm irrigation\r\nCommon People- For domestic use etc.\r\nWe provide complete boring/drilling service by involving our own associated materials like submersible motor pumps, reqd. pipes (like MS, GI, PVC or column), panel (starter), wires etc.\r\n\r\nBeing a responsible and trusted firm we offer free maintenance service for 1 yr after the project and 3 yrs of warranty on products.\r\n*T&C Apply.\r\nFor more detail contact us.\r\n\r\n\r\nIntroduction To ODEX Casing System\r\n\r\nThe diagrams below show a ODEX Casing system in both the drilling mode as well as the retract mode. Also shown is a ODEX Sampling system that would be used for Placer Sampling of overburden. We have in stock 5\", 7\", and 8\" ODEX systems. Other sizes are available on request. In our water exploration drilling we always set ODEX casing at the top of the hole until bedrock is encountered. This helps reduce caving of the hole and helps direct the sample into the center of the drill pipe where it belongs by the use of a Sealed Diverter between the casing and the drill rod.', 'About'),
(11, 1, 'http://shivatubewells.com/images/slide1.jpg', 'About'),
(12, 1, 'http://shivatubewells.com/images/slide2.jpg', 'About'),
(13, 1, 'http://shivatubewells.com/images/slide3.jpg', 'About'),
(14, 1, 'http://shivatubewells.com/images/slide4.jpg', 'About'),
(15, 1, 'http://shivatubewells.com/images/slide5.jpg', 'About'),
(16, 1, 'http://shivatubewells.com/images/slide6.jpg', 'About'),
(17, 1, 'http://shivatubewells.com/images/img1.jpg', 'About'),
(18, 1, 'http://shivatubewells.com/images/odex1.jpg', 'About'),
(19, 1, 'http://shivatubewells.com/images/odex2.jpg', 'About'),
(20, 1, 'http://shivatubewells.com/images/img2.jpg', 'About'),
(21, 1, 'http://shivatubewells.com/images/img4.jpg', 'About'),
(22, 0, 'Our long term vision and commited service makes us stand out of the crowd. Being a responsible firm we understand your requirement and thats why there is a maintenance department in our firm who assures your after salses service.', 'Services'),
(23, 1, 'http://shivatubewells.com/images/slides/1.jpg', 'Services'),
(24, 1, 'http://shivatubewells.com/images/slides/2.jpg', 'Services'),
(25, 1, 'http://shivatubewells.com/images/slides/3.jpg', 'Services'),
(26, 1, 'http://shivatubewells.com/images/slides/4.jpg', 'Services'),
(27, 1, 'http://shivatubewells.com/images/slides/5.jpg', 'Services'),
(28, 1, 'http://shivatubewells.com/images/slides/6.jpg', 'Services'),
(30, 0, 'WELCOME TO OUR PORTFOLIO PAGE. THE PAGE INCLUDES SOME OF OUR CLIENTS AND RECENT PROJECT SLIDES\r\n\r\n   \r\n    \r\nLARGEST BOREWELL DEVELOPER IN UTTARAKHAND\r\nMore than 1000 commercial,private/individual,government tubewells delivered since 2004\r\n\r\nOur recent borewell projects inlude Doiwala flyover bridge, multiple factories at Langha, multiple aashrams and hotels at Rishikesh, Hill View Apartment Sahastradhara, Mars Catering Mussoorie, Kalandi Medco, Poly houses at Herbertpur, Sahaspur and Badowala, Kukreja Inst, Corel Lab, Amber Interprises etc\r\n\r\nOUR CLIENTS\r\nAREA SERVED\r\n \r\nEvery region lying at the Himalayan belt are served. Instances below\r\n	       \r\nAjabpur         Koti		\r\n\r\nMahebawalla	Barautha\r\n\r\nHerbetpur 	Bariowganj\r\n\r\nVikasnagar 	Bhaniawala\r\n\r\nSelaqui 	Bhogpur	\r\n\r\nPrem Nagar 	Bhidoli\r\n\r\nBhauwala 	Chakrata \r\n\r\nRishikesh 	Chibroo\r\n\r\nRanipur 	Clement Town\r\n\r\nDoiwala 	Dakpathar\r\n\r\nKangri          Dehradun Cantt \r\n\r\nNepali Farm 	Dhakrani \r\n\r\nJolly grant 	Dehradun 		\r\n\r\nHathiBarkla         Balawala \r\n\r\nGhanghora	and many more...\r\n\r\nHarrawalla	\r\n\r\nHerbertpur	\r\n\r\nJharipani	\r\n\r\nKalsi	\r\n\r\nKolagarh	\r\n\r\n\r\n\r\n \r\nDEMONSTRATION\r\n \r\n\r\n\r\n\r\n \r\nGive us a call at\r\n+91 8869081529, +91 9412938365', 'Portfolio'),
(31, 1, 'http://shivatubewells.com/css/images/1.jpg', 'Portfolio'),
(32, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio'),
(33, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio'),
(34, 1, 'http://shivatubewells.com/css/images/3.jpg', 'Portfolio'),
(35, 1, 'http://shivatubewells.com/css/images/4.jpg', 'Portfolio'),
(36, 1, 'http://shivatubewells.com/css/images/5.jpg', 'Portfolio'),
(37, 1, 'http://shivatubewells.com/css/images/6.jpg', 'Portfolio'),
(39, 1, 'http://shivatubewells.com/css/images/7.jpg', 'Portfolio'),
(40, 1, 'http://shivatubewells.com/css/images/8.jpg', 'Portfolio'),
(41, 1, 'http://shivatubewells.com/css/images/9.jpg', 'Portfolio'),
(42, 1, 'http://shivatubewells.com/css/images/10.jpg', 'Portfolio'),
(43, 1, 'http://shivatubewells.com/css/images/11.jpg', 'Portfolio'),
(44, 1, 'http://shivatubewells.com/css/images/12.jpg', 'Portfolio'),
(45, 1, 'http://shivatubewells.com/css/images/13.jpg', 'Portfolio'),
(46, 1, 'http://shivatubewells.com/css/images/14.jpg', 'Portfolio'),
(47, 1, 'http://shivatubewells.com/css/images/15.jpg', 'Portfolio'),
(48, 1, 'http://shivatubewells.com/css/images/16.jpg', 'Portfolio'),
(49, 1, 'http://shivatubewells.com/css/images/17.jpg', 'Portfolio'),
(50, 1, 'http://shivatubewells.com/css/images/18.jpg', 'Portfolio'),
(51, 1, 'http://shivatubewells.com/css/images/19.jpg', 'Portfolio'),
(52, 1, 'http://shivatubewells.com/css/images/20.jpg', 'Portfolio'),
(53, 1, 'http://shivatubewells.com/css/images/21.jpg', 'Portfolio'),
(54, 1, 'http://shivatubewells.com/css/images/22.jpg', 'Portfolio'),
(55, 1, 'http://shivatubewells.com/css/images/23.jpg', 'Portfolio'),
(56, 1, 'http://shivatubewells.com/css/images/24.jpg', 'Portfolio'),
(57, 1, 'http://shivatubewells.com/images/slide1.jpg', 'Contacts'),
(58, 1, 'http://shivatubewells.com/images/slide2.jpg', 'Contacts'),
(59, 1, 'http://shivatubewells.com/images/slide3.jpg', 'Contacts'),
(60, 1, 'http://shivatubewells.com/images/slide4.jpg', 'Contacts'),
(61, 1, 'http://shivatubewells.com/images/slide5.jpg', 'Contacts'),
(62, 1, 'http://shivatubewells.com/images/slide6.jpg', 'Contacts'),
(63, 1, 'http://shivatubewells.com/images/slide6.jpg', 'Contacts'),
(64, 0, '\r\nHead Office : Prem Nagar\r\nBranch Office\r\n\r\nJhajra, Near Balaji Temple\r\nZip Code:248197\r\nCountry:India\r\nCity:Dehradun\r\nCell no:+91 8869081529\r\nCell no:+91 9412938365\r\nEmail:info@shivatubewells.com\r\n\r\nContact Us\r\nCall us or leave us a mail. We\'d be glad to assist', 'Contact'),
(65, 0, 'Contact Form\r\nFields marked with * are mandatory.\r\nFull Name *	\r\nEmail Address *	\r\nContact Number	\r\nYour Message *	\r\nTo help prevent automated spam, please answer this question\r\n\r\n* Using only numbers, what is 10 + 15?   \r\nEnter your answer here', 'Contact');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `serial_number` varchar(25) DEFAULT NULL,
  `item_name` text CHARACTER SET latin1 NOT NULL,
  `unit_rate` int(11) NOT NULL,
  `particulars` text DEFAULT NULL,
  `unit` varchar(10) NOT NULL,
  `quantity` int(7) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `cgst` tinyint(2) DEFAULT NULL,
  `sgst` tinyint(2) DEFAULT NULL,
  `igst` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `serial_number`, `item_name`, `unit_rate`, `particulars`, `unit`, `quantity`, `description`, `created_at`, `cgst`, `sgst`, `igst`) VALUES
(1, '1', 'name', 951, '1', '1', 1, 'description', '2019-07-02 10:19:22', 1, 1, 1),
(2, NULL, 'name', 951, NULL, '1', 1, 'description', '2019-07-02 10:31:37', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `site` text NOT NULL,
  `bore_dia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `name`, `email`, `contact`, `site`, `bore_dia`) VALUES
(1, 'name', 'emewtgfergail@gmail.com', 9852369874, 'site', '5cm'),
(2, 'name', 'emewtgfergail@gmail.com', 9852369874, '', ''),
(3, 'name', 'sfytgfd@gmail.com', 9852369874, '', ''),
(4, 'mohan', 'mohan@gmail.com', 7814016006, 'www.google.com', '53'),
(5, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"'),
(6, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"'),
(7, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"'),
(8, 'name', 'email@email.com', 9475862134, 'Dehradun', '5');

-- --------------------------------------------------------

--
-- Table structure for table `zonal_data`
--

CREATE TABLE `zonal_data` (
  `id` int(11) NOT NULL,
  `area` text NOT NULL,
  `bore_depth` int(6) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person` (`id`,`created_at`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zonal_data`
--
ALTER TABLE `zonal_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`created_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `zonal_data`
--
ALTER TABLE `zonal_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
