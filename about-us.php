<?php
$page="about";
include('header.php');
?>
<title>About us</title>

<section class="bg-white margin-bottom-sm">
<!-- Scroll to top -->
<div class="float-right top_bottom_scroll p-3">
<a id="" href="#bottomscroll" class="btn btn-danger2" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
      <i class="fas fa-chevron-down"></i>
</a>
</div>
<!-- Scroll to top -->
<section class="container pt-5" id="startchange">
    <div class="row">
        <div class="col-sm-6">
            <img src="images/img5.jpg" alt="water drilling firm dehradun" class="border">
        </div>
        <div class="col-sm-6 my-auto">
            <h2>A MESSAGE FROM OUR PRESIDENT</h2>
            <p>We are Dehradun based govt. regd. leading water drilling firm. We are announced officially in the year
                2004 but we've been working much earlier.We hold vast drilling experience. We have expertise in 5, 7, &
            8" Odex drilling.</p>
        </div>
    </div>
</section>

<section class="container mt-5">
    <div class="row text-justify">
        <div class="col-sm-6">
            <strong>Our targeted customer belongs to the following categories:-</strong>
            <p>Farmers-Agriculture<br>
                Industrialists-Plants and Machinery industry<br>
                Builders-Real estate (building construction)<br>
                Government- Civil construction (like bridge, flyover, road, drainage construction or hand pump
                installations)<br>
                Cultivation- Nursery, farm irrigation<br>
            Common People- For domestic use etc.</p>
            <p>We provide complete boring/drilling service by involving our own associated materials like submersible
            motor pumps, reqd. pipes (like MS, GI, PVC or column), panel (starter), wires etc.</p>
            <p>Being a responsible and trusted firm we offer free maintenance service for 1 yr after the project and 3
            yrs of warranty on products. *T&C Apply.</p>
            <p>For more detail go <a href="contact-us.php">Contact Us</a> page.</p>
        </div>
        <div class="col-sm-6">
            <h4 class="font-weight-normal">Introduction To ODEX Casing System</h4>
            <p>The diagrams below show a ODEX Casing system in both the drilling mode as well as the retract mode. Also
                shown is a ODEX Sampling system that would be used for Placer Sampling of overburden. We have in stock
                5", 7", and 8" ODEX systems. Other sizes are available on request. In our water exploration drilling we
                always set ODEX casing at the top of the hole until bedrock is encountered. This helps reduce caving of
                the hole and helps direct the sample into the center of the drill pipe where it belongs by the use of a
            Sealed Diverter between the casing and the drill rod.</p>
        </div>
    </div>
</section>

<section class="bg-clr-set mb-0">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h2>We're Your Underground Construction Specialists</h2>
                <p>Learn More About <a href="about-us.php">Who We Are</a> | <a href="services.php">Services</a> | <a
                    href="portfolio.php">Portfolio</a> | <a href="contact-us.php">Contact With Us</a></p>
                </div>
                <div class="col-lg-6">
                    <div class="text-center">
                        <button type="button" class="btn btn-danger1 btn-lg bg-white txt-btn">
                            <a href="#bottomscroll" class="text-dark text-decoration-none px-2">LET'S CONNECT</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php /*<section class="container py-5">
        <h2 class="text-center font-weight-bold mb-4">ABOUT OUR GOOD TEAM LEADERS</h2>
        <div class="row">
            <div class="col-lg-3 col-md-6 mt-4">
                <div class="text-center">
                    <h4>Mr. Nivesh Anand</h4>
                    <strong class="font-weight-normal"> IT Dept & Public relations </strong>
                    <p>10 Years of Industry Experience.</p>
                    <button type="button" class="btn btn-lg txt-btn-about">
                        <a href="" class="text-danger1 text-decoration-none px-1">CONTACT NIVESH</a>
                    </button>
                </div>
            </div><!-- col -->

            <div class="col-lg-3 col-md-6 mt-4">
                <div class="text-center">
                    <h4>Mr. Sudhakar</h4>
                    <strong class="font-weight-normal"> Accounts </strong>
                    <p>10 Years of Industry Experience.</p>
                    <button type="button" class="btn btn-lg txt-btn-about">
                        <a href="" class="text-danger1 text-decoration-none px-1">CONTACT SHUDHAKER</a>
                    </button>
                </div>
            </div><!-- col -->

            <div class="col-lg-3 col-md-6 mt-4">
                <div class="text-center">
                    <h4>Mr. Nivesh Anand</h4>
                    <strong class="font-weight-normal"> IT Dept & Public relations </strong>
                    <p>10 Years of Industry Experience.</p>
                    <button type="button" class="btn btn-lg txt-btn-about">
                        <a href="" class="text-danger1 text-decoration-none px-1">CONTACT NIVESH</a>
                    </button>
                </div>
            </div><!-- col -->

            <div class="col-lg-3 col-md-6 mt-4">
                <div class="text-center">
                    <h4>Mr. Nivesh Anand</h4>
                    <strong class="font-weight-normal"> IT Dept & Public relations </strong>
                    <p>10 Years of Industry Experience.</p>
                    <button type="button" class="btn btn-lg txt-btn-about">
                        <a href="" class="text-danger1 text-decoration-none px-1">CONTACT NIVESH</a>
                    </button>
                </div>
            </div><!-- col -->
        </div><!-- row -->
    </section> */?>

    <section class="back-img-set mt-0">
        <div class="container">
            <div class="text-white m-auto text-center">
                <h3>Help our clients achieve their goals.</h3>
                <h3>We help our trade partners grow and succeed.</h3>
                <h3>Building long-term relationships out of reliability, honesty, and trust.</h3>
                <button type="button" class="btn btn-danger1 btn-lg txt-btn my-4">
                    <a href="" class="text-white text-decoration-none pX-2">LET'S ACHIEVE YOUR GOALS,TOGETHER</a>
                </button>
            </div>
        </div>
    </section>
    <section class="container py-5" id="bottomscroll">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="https://cruxmagic.com/trust-seal" class="text-mob">Trust Seal</a></p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" alt="best quality borewell work dehradun" class="trust-seal" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>

        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
</section>
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
</section>
</section>
    <?php
    include('footer.php');
    ?>
