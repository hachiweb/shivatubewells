<?php
$page="service";
include('header.php');
?>

<section class="bg-white margin-bottom-sm">
<!-- Scroll to top -->
<div class="float-right top_bottom_scroll p-3">
<a id="" href="#bottomscroll" class="btn btn-danger2" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
      <i class="fas fa-chevron-down"></i>
</a>
</div>
<!-- Scroll to top -->
<section class="container" id="startchange">
    <div class="row m-auto text-justify text-center padd-set-shiva">
        <h3 class="text-center m-auto">SHIVATUBEWELLS SERVICES</h3>
        <p class="mt-4">Our long term vision and commited service makes us stand out of the crowd.
        Being a responsible firm we understand your requirement and thats why there is a maintenance department in our firm who assures your after salses service.</p>
        <div class="m-auto">
            <button type="button" class="btn btn-danger1 btn-lg txt-btn mt-5">
                <a href="contact-us.php" class="text-white text-decoration-none px-2">REGISTER NOW</a>
            </button>
        </div>
    </div>
</section>
<section class="container-fluid mt-4">
    <div class="row content-justify margin-set">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images1 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images2 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images3 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images4 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images5 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="back-set-image set-images6 common-overlay">
                <div class="zindex-1">
                    <h2>ODEX/DTH</h2>
                    <p>This is what we do best! Let us take the work off your hands! We’ll start with a thorough site inspection of your outside plant installation and handle your project from point of origination to point of connection.</p>
                    <a href="" class="">Learn more</a>
                </div>
            </div>
        </div>
    </section>
    <section class="container py-4" id="">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="https://cruxmagic.com/trust-seal" class="text-mob">Trust Seal</a></p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" alt="shivatubewell dehradun" class="trust-seal" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>

        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
</section>
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
</section>
<div id="bottomscroll"></div>
</section>
    <?php
    include('footer.php');
    ?>
