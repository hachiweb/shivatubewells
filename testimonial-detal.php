<?php
$page="testimonial";
include('header.php');
?>
<section class="bg-white margin-bottom-sm">
<section class="testimonial-detal-sec common-sec">
    <!-- Scroll to top -->
<div class="float-right top_bottom_scroll p-3">
<a id="" href="#bottomscroll" class="btn btn-danger2" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
      <i class="fas fa-chevron-down"></i>
</a>
</div>
<!-- Scroll to top -->
    <div class="container" id="startchange">
        <h1 class="entry-title">ODEX / DTH Drilling</h1>

        <div class="row">
            <div class="col-lg-8">
                <div class="entry-content">
                    <div class="img">
                        <img src="images/testimonials/dth-drlling.jpg" alt="DTH Hydroulic drill dehradun">
                    </div>

                    <p>ODEX is an excellent option when unconsolidated formations are too dense or cobbly for Auger Drilling. ODEX is a down-hole air hammer system that is designed to advance casing during drilling. </p>

                    <p>Once a desired depth is reached they eccentric bit can be retrieved leaving the casing in place for sampling or installations.
                    When the bore-hole is complete, the casing is retrieved to be used again.</p>
                </div><!-- entry-content -->
            </div><!-- col -->

            <div class="col-lg-4">
                <div class="card">
                  <div class="iframe card-img-top">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13772.926003263405!2d77.918622!3d30.34437!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcff1c8a5d66a45f8!2sShiva%20Tubewells(Borewell%20Constructions%20Dehradun)!5e0!3m2!1sen!2sin!4v1570155762479!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title">Shiva Tubewells(Borewell Constructions Dehradun)</h4>
                    <div class="review">
                        <div class="ratio">(4.6)</div>
                        <div class="star">
                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                            <span><i class="fa fa-star-half-o" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Borewell / Tubewell contractor in Dehradun, Uttarakhand</li>
                    <li class="list-group-item"><b>Address:</b> Near Balaji Dham, Jhajra, Dehradun, Uttarakhand 248197</li>
                    <li class="list-group-item"><b>Phone:</b> <a href="tel:+91 88690 81529" style="color:#022052; text-decoration:none" >+91 88690 81529</a></li>
                    <li class="list-group-item">
                        <div class="timing-bx">
                            <div><b>Timing:</b></div>
                            <div>
                                <span class="days mr-3">Mon - Saturday</span>
                                <span>8:00 am - 8:30 pm</span>
                            </div>
                            <div>
                                <span class="days mr-3">Sunday</span>
                                <span>Closed</span>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="card-body">
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div>
        </div><!-- col -->
    </div><!-- row -->

</div><!-- container -->
</section><!-- testimonial-detal-sec -->

<section class="testimonials-sec">
    <div class="container">
        <div class="testimonial-bx">

            <div class="testimonials-dv">
                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108248202436083305644/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAS">
                                        Aman Batra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Amazing services. Got more than what I paid. Very professional and much recommended.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108248202436083305644/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAS">
                                    <img src="images/testimonials/photo.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/113530444860659626471/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAZ">
                                        Vimal Devrari
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Very good work by Shiva tube well team!!  There behaviour, hard work and focus on there work is outstanding!!  U should contact them for drilling!!   </p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/113530444860659626471/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAZ">
                                    <img src="images/testimonials/photo1.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/101259021679572820440/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAg">
                                        Nand kishor Malhotra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>He give us very good service and helpful good  rate for boring</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/101259021679572820440/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAg">
                                    <img src="images/testimonials/photo2.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/117435212400259742287/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAn">
                                        Ashish Puri
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Shiva Tubewells are well versed in their services. Their drilling equipment and technology is very nice. Also, their team is very firm and quick at task. Good job..!!</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/117435212400259742287/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAn">
                                    <img src="images/testimonials/photo3.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108647230645944677346/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAu">
                                        Deepak Prajapati
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Awesome👏✊👍</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108647230645944677346/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAu">
                                    <img src="images/testimonials/photo4.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/100334357424873365805/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA1">
                                        Sameer Arora
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Undoubtedly very fast service with affordable machinery parts. Their team is very supporting and helpful..!!</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/100334357424873365805/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA1">
                                    <img src="images/testimonials/photo5.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/116434053513280877021/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA8">
                                        Zoya Akhtar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>One of the best service providers in Rishikesh. They have a very dedicated team for their work.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/116434053513280877021/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA8">
                                    <img src="images/testimonials/photo6.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111267024750297042963/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBD">
                                        Kunal Mehta
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Good job done, you guys really helped us. Will recommend to others.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111267024750297042963/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBD">
                                    <img src="images/testimonials/photo7.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/110964976779591695750/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBK">
                                        Heena Kalra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>I was looking for a tubewell boring contractor in Haridwar and get in touch with Shiva Tubewells. They provided us fast service with all the additional machinery needed at a reasonable price. Recommended for any kind of drilling services..</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/110964976779591695750/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBK">
                                    <img src="images/testimonials/photo8.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/116370404157529843675/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBR">
                                        Pinkesh Tomar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Wonderful work and brilliant team of Shiva Tube wells</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/116370404157529843675/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBR">
                                    <img src="images/testimonials/photo9.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/102911228534801445829/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAC">
                                        pavan kumar U
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Their service is really good. Can totally rely on the tubewell products here.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/102911228534801445829/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAC">
                                    <img src="images/testimonials/photo10.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/117232992997505591885/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAJ">
                                        SHIV GANGA TUBEWELLS
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>very good services</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/117232992997505591885/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAJ">
                                    <img src="images/testimonials/photo11.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/109491887894467805369/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAQ">
                                        SANKARSHAN TRIPATHI
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Best for deliver long term as well as short term borewell construction.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/109491887894467805369/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAQ">
                                    <img src="images/testimonials/photo12.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/112861573939838237585/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAX">
                                        Magin Ranjit
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>"Delivers what it promises"-  Got my 8 inch borewell developed over night at Hathibarkla, Dehradun 248001</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/112861573939838237585/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAX">
                                    <img src="images/testimonials/photo13.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111365297219546722394/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAe">
                                        Shiva Tubewells
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Our motto is to deliver our best.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111365297219546722394/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAe">
                                    <img src="images/testimonials/photo14.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/112919184390109621646/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAl">
                                        Rajnish Nigam
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Helpful</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/112919184390109621646/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAl">
                                    <img src="images/testimonials/photo15.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108550199442617753923/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAs">
                                        Ajay Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>बहुत अच्छा काम करते है।</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108550199442617753923/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAs">
                                    <img src="images/testimonials/photo16.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/115398594173184926157/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAz">
                                        Prateek Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/115398594173184926157/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAz">
                                    <img src="images/testimonials/photo17.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/103579751596339231271/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA3">
                                        Utkarsh singh
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/103579751596339231271/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA3">
                                    <img src="images/testimonials/photo18.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/100166325659664321249/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA6">
                                        Wotoki Awomi
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/100166325659664321249/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA6">
                                    <img src="images/testimonials/photo19.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/106404646044966935557/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAC">
                                        Ranjeet Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/106404646044966935557/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAC">
                                    <img src="images/testimonials/photo20.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111828459895964881075/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAF">
                                        Amardeep Gulati
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111828459895964881075/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAF">
                                    <img src="images/testimonials/photo21.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/101872532016271317128/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAI">
                                        Mohan Sai Manthri
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/101872532016271317128/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAI">
                                    <img src="images/testimonials/photo22.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/106891134047049238234/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAM">
                                        leeway handikraft
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/106891134047049238234/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAM">
                                    <img src="images/testimonials/photo23.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

            </div><!-- testimonials -->

        </div><!-- testimonial-bx -->
    </div><!-- container -->
</section><!-- testimonials-sec -->

<section class="container py-5" id="">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="https://cruxmagic.com/trust-seal" class="text-mob">Trust Seal</a></p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" class="trust-seal" alt="Borewell Constructions Dehradun" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>

        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
</section>
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
</section>
<div id="bottomscroll"></div>
</section>
<?php include('footer.php'); ?>

</body>

</html>