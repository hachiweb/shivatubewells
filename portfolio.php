<?php
$page="portfolio";
include('header.php');
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<section class="bg-white margin-bottom-sm">
<!-- Scroll to top -->
<div class="float-right top_bottom_scroll p-3">
<a id="" href="#bottomscroll" class="btn btn-danger2" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
      <i class="fas fa-chevron-down"></i>
</a>
</div>
<!-- Scroll to top -->
<section class="container" id="startchange">
    <div class="m-auto text-justify text-center padd-set-shiva">
        <h4 class="text-center m-auto">WELCOME TO OUR PORTFOLIO PAGE.</h4>
        <h5 class="text-center m-auto font-weight-normal">THE PAGE INCLUDES SOME OF OUR CLIENTS AND RECENT PROJECTS</h5>
        <p class="m-auto">More than 1000 commercial,private/individual,government tubewells delivered since 2004.</p>
        <p class="">Our recent borewell projects inlude Doiwala flyover bridge, multiple factories at Langha, multiple aashrams and hotels at Rishikesh, Hill View Apartment Sahastradhara, Mars Catering Mussoorie, Kalandi Medco, Poly houses at Herbertpur, Sahaspur and Badowala, Kukreja Inst, Corel Lab, Amber Interprises etc.</p>


    </div>
</section>

<section class="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/1.jpg">
                    <img src="images/1.jpg" alt="happy client for our services dehradun">
                 </a>        
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/2.jpg">
                    <img src="images/2.jpg" alt="sidcul selaqui shivatubewell dehradun">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/3.jpg">
                    <img src="images/3.jpg" alt="grand meedos borewell dehradun">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/4.jpg">
                    <img src="images/4.jpg" alt="best borewell contractor Uttarakhand">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/5.jpg">
                    <img src="images/5.jpg" alt="tubewell contractor uttrakhand">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/6.jpg">
                    <img src="images/6.jpg" alt="odex drilling services uttrakhand">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/7.jpg">
                    <img src="images/7.jpg" alt="best 5, 7, 8 inch ODEX drilling dehradun">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/8.jpg">
                    <img src="images/8.jpg" alt="best borewell services dehradun">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/9.jpg">
                    <img src="images/9.jpg" alt="best ODEX drilling dehradun">
                 </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/10.jpg">
                    <img src="images/10.jpg" alt="ODEX drilling uttrakhand"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/11.jpg">
                    <img src="images/11.jpg" alt="borewell work dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/12.jpg">
                    <img src="images/12.jpg" alt="best 5, 7, 8 inch drilling uttrakhand"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/13.jpg">
                    <img src="images/13.jpg" alt="tubewell contractor Dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/14.jpg">
                    <img src="images/14.jpg" alt="borewell contractor Uttarakhand"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/15.jpg">
                    <img src="images/15.jpg" alt="odex drilling dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/16.jpg">
                    <img src="images/16.jpg" alt="happy client submersible dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/17.jpg">
                    <img src="images/17.jpg" alt="government tubewells delivered dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/18.jpg">
                    <img src="images/18.jpg" alt="individual tubewells delivered dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/19.jpg">
                    <img src="images/19.jpg" alt="private tubewells delivered dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/20.jpg">
                    <img src="images/20.jpg" alt="commercial tubewells delivered dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/21.jpg">
                    <img src="images/21.jpg" alt="best commercial tubewells dehradun"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/22.jpg">
                    <img src="images/22.jpg" alt="best work borewell projects uttrakhand"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/23.jpg">
                    <img src="images/23.jpg" alt="best work submersible projects uttrakhand"> 
                </a>
            </div><!-- col -->

            <div class="col-lg-3 col-md-3 col-sm-2 mt-4">
                <a data-fancybox="gallery" href="images/24.jpg">
                    <img src="images/24.jpg" alt="best private tubewells delivered dehradun"> 
                </a>
            </div><!-- col -->
        </div><!-- row -->
    </div><!-- container-fluid -->
</section>

<section class="bg-clr-setet">
    <div class="container">
        <h1 class="m-auto text-center text-white">AREA SERVED</h1>
        <div class="row btn-txt-set">
            <div class="col-sm-3">
                <ul>
                    <li><i class="far fa-dot-circle text-success"></i>Ajabpur</li>
                    <li><i class="far fa-dot-circle text-success"></i>Koti</li>
                    <li><i class="far fa-dot-circle text-success"></i>Mahebawalla</li>
                    <li><i class="far fa-dot-circle text-success"></i>Barautha</li>
                    <li><i class="far fa-dot-circle text-success"></i>Herbetpur</li>
                    <li><i class="far fa-dot-circle text-success"></i>Bariowganj</li>
                    <li><i class="far fa-dot-circle text-success"></i>Vikasnagar</li>
                    <li><i class="far fa-dot-circle text-success"></i>Bhaniawala</li>
                    <li><i class="far fa-dot-circle text-success"></i>Selaqui</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul>
                    <li><i class="far fa-dot-circle text-success"></i>Prem Nagar</li>
                    <li><i class="far fa-dot-circle text-success"></i>Bhidoli</li>
                    <li><i class="far fa-dot-circle text-success"></i>Bhauwala</li>
                    <li><i class="far fa-dot-circle text-success"></i>Chakrata</li>
                    <li><i class="far fa-dot-circle text-success"></i>Risikesh</li>
                    <li><i class="far fa-dot-circle text-success"></i>Chibroo</li>
                    <li><i class="far fa-dot-circle text-success"></i>Ranipur</li>
                    <li><i class="far fa-dot-circle text-success"></i>Clement Town</li>
                    <li><i class="far fa-dot-circle text-success"></i>Doiwala</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul>
                    <li><i class="far fa-dot-circle text-success"></i>Dehradun Cantt</li>
                    <li><i class="far fa-dot-circle text-success"></i>Nepali Farm</li>
                    <li><i class="far fa-dot-circle text-success"></i>Dhakrani</li>
                    <li><i class="far fa-dot-circle text-success"></i>Jolly grant</li>
                    <li><i class="far fa-dot-circle text-success"></i>Dehradun</li>
                    <li><i class="far fa-dot-circle text-success"></i>HathiBarkla</li>
                    <li><i class="far fa-dot-circle text-success"></i>Balawala</li>
                    <li><i class="far fa-dot-circle text-success"></i>DoGhanghoraiwala</li>
                    <li><i class="far fa-dot-circle text-success"></i>Harrawalla</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul> 
                <li>
                    <i class="far fa-dot-circle text-success"></i>Kangri</li>
                    <li><i class="far fa-dot-circle text-success"></i>Dakpathar</li>
                    <li><i class="far fa-dot-circle text-success"></i>Bhogpur</li>
                    <li><i class="far fa-dot-circle text-success"></i>Herbertpur</li>
                    <li><i class="far fa-dot-circle text-success"></i>Jharipani</li>
                    <li><i class="far fa-dot-circle text-success"></i>Kalsi</li>
                    <li><i class="far fa-dot-circle text-success"></i>Kolagarh</li>
                    <li><i class="far fa-dot-circle text-success"></i>and many more...</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="container py-5" id="">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="https://cruxmagic.com/trust-seal" class="text-mob">Trust Seal</a></p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" alt="happy client borewell dehradun" class="trust-seal" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>

        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
</section>
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
</section>
<div id="bottomscroll"></div>
</section>

<?php
include('footer.php');
?>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
