
<section class="sticky">
<div class="bg-color-footer">
    <div class="container txt-center">
    <!-- <div class="fb-like" data-href="https://www.facebook.com/pages/SHIVA-Tubewells/607451195950465?pnref=lhc" data-layout="standard" data-action="like" data-show-faces="true" data-share="true">facebook like</div> -->
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
            <h4 class="pt-2"><u>About Shiva Tubewells</u></h4>
                <p class="text-white text-justify">We are Dehradun based govt. regd. leading water drilling firm. We are announced officially in the year 2004, though we have been in the industry for even longer. We hold vast drilling experience with expertise in 5, 7, & 8 inch ODEX / DTH drilling.</p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="">
                    <h4 class="p-2"><u>Contact Shiva Tubewells</u></h4>
                    <h6 class="px-2"><span><i class="fas fa-phone-volume text-success"></i></span> +91 8869081529</h6>
                    <h6 class="p-2"><span><i class="fas fa-phone-volume text-success"></i></span> +91 9412938365</h3>
                    <p class="p-2">Shiva Tubewells, Jhajra, Near Balaji Temple, Dehradun</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
            <h4 class=""><u>Shiva Tubewells Services</u></h4>
            <p class="text-white"><a href="services.php" class="text-white">SERVICES</a></p>
            <script language="JavaScript" type="text/javascript">
                TrustLogo("https://www.shivatubewells.com/images/instantssl_trust_seal/instantssl_trust_seal_lg_210x54.png", "SC5", "none");
            </script>
           
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="social-icon">
                    <a href="https://www.facebook.com/shiva.tubewells">
                        <i class="fab fa-facebook-square fas1"></i>
                    </a>
                    <a href="https://twitter.com/shivatub">
                        <i class="fab fa-twitter-square fas2"></i>
                    </a>
                    <a href="linkedin.com/in/shiva-tubewells-8717a870">
                        <i class="fab fa-linkedin fas2"></i>
                    </a>
                    <a href="https://en-gb.facebook.com/pg/shivatub/posts/">
                        <i class="fab fa-instagram fas2"></i>
                    </a>
                </div>
                <div class="appstoreIcon">
                    <a href="https://play.google.com/store/apps/details?id=com.shivatubewells.borewelldehradun&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"> 
                        <img src="images/google-play-badge.png" alt="shiva tubewells android app" id="google" class="img-fluid"> 
                    </a>
                </div>
            </div>
        </div>
            <div class="row mt-5">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="text-center">
                <p class="text-white"> <a href="/" class="text-white">HOME</a> | <a
                    href="about-us.php" class="text-white">ABOUT US</a> | <a href="services.php"
                    class="text-white">SERVICES</a> | <a href="portfolio.php" class="text-white">PORTFOLIO</a> | <a
                    href="contact-us.php" class="text-white">CONTACT US</a> | <a href="" class="text-white">CAREER</a>
                </p>
                
                </div>
            </div>
        </div>
    </div>
</div>
<div style="background-color:#0e305f; padding:10px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="d-flex justify-content-around mt-2">
                    <p class="text-white" style="font-size:16px;">Copyright &copy; 2013-2019<a href="#"
                        class="text-success"> Shiva Tubewells.com</a>
                    - All Rights Reserved</p>
                    <!-- <p class="text-white" style="">Designed and maintained by: <a href="http://hachiweb.com" target="_blank" class="text-success">Hachiweb<a><p> -->
                </div>
            </div>
        </div>
    </div>
</div> 
</section>  
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>

function get_action(form) 
{
    var v = grecaptcha.getResponse();
    if(v.length == 0)
    {
        document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
        return false;
    }
    else
    {
         document.getElementById('captcha').innerHTML="Captcha completed";
        return true; 
    }
}
</script>
    <script>
        AOS.init();
        $(document).ready(function(){
        $('[data-toggle="popover"]').popover();   
        // Contact-form validation
            $(".Contact_Number").keypress(function (e) {
                var length = $(this).val().length;
                if(length > 11) {
                        return false;
                } else if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                } else if((length == 1) && (e.which == 48)) {
                        return false;
                }
            });
        });

        //Scroll sticky
        $(window).scroll(function(){
            var sticky = $('.sticky'),
                scroll = $(window).scrollTop();

            if (scroll >= 100) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-43972295-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-43972295-1');
    </script>
</body>
</html>