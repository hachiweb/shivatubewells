// custom js
$(document).ready(function(){
	  $(document).click(function(){
		$(".collapse").slideUp(700);
	  });
	  $(".navbar-toggler").click(function(){
		$(".collapse").slideToggle(800);
	  });

	  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

	if (w>800) {
		$(document).on('mouseover click',".android-image>a>img", function (){
			$(".android-image-gif>a>img").show();
			$(".android-image>a>img").hide();
		});
		
		$(document).on('mouseout click',".android-image>a>img", function (){
			$(".android-image-gif>a>img").hide();
			$(".android-image>a>img").show();
		});
		}

		else {
		$(document).on('click',".android-image>a>img", function (){
			$(".android-image-gif>a>img").show();
			$(".android-image>a>img").hide();
		});
		
		$(document).on('click',".android-image>a>img", function (){
			$(".android-image-gif>a>img").hide();
			$(".android-image>a>img").show();
		});
	}

	$('.testimonials-dv').slick({
		dots: false,
		infinite: true,
		arrows: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});


// document ready end 
});

$(function() {
	$( 'ul.nav li' ).on( 'click', function() {
		  $( this ).parent().find( 'li.active' ).removeClass( 'active' );
		  $( this ).addClass( 'active' );
	});
});