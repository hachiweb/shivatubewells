<?php
$page="contact";
include('header.php');
?>
<!-- <div class="bg-img common-overlay" style="background-image:url('images/shivalik-tubewells.jpg');background-size: cover;background-repeat: no-repeat;">
    <div class="container zindex-1">
        <div class="text-center">
            <h4>We are Underground Specialists</h4>
            <h1>RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME.</h1>
            <h5>Connect With Us</h5>
        </div>
    </div>
</div> -->
<!-- <img class="d-block w-100" src="images/shivalik-tubewells.jpg" data-color="lightblue" -->
<!-- Scroll to top -->
<section class="bg-white margin-bottom-sm">

<div class="float-right top_bottom_scroll p-3" id="startchange">
<a id="" href="#bottomscroll" class="btn btn-danger2" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
      <i class="fas fa-chevron-down"></i>
</a>
</div>
<section class="container py-5" id="">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <div class="fb-page" data-href="https://www.facebook.com/shivatub/" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shivatub/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shivatub/">SHIVA Tubewells</a></blockquote></div>
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" class="trust-seal" alt="best guaranty borewell contractor dehradun" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
    
    <!-- <div id="fb-root"></div> -->
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=265521817231624&autoLogAppEvents=1">
    </script>
</section>
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
<div id="bottomscroll"></div>
</section>
</section>
<!-- Scroll to top -->
<?php
include('footer.php');
?>