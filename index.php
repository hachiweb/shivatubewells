<?php
$page="home";
include('header.php');
?>

 <div class="android-image">
 <a href="https://play.google.com/store/apps/details?id=com.shivatubewells.borewelldehradun&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="images/android.png" alt="borewell dehradun"></a>
</div>

<div class="android-image-gif">
    <a href="https://play.google.com/store/apps/details?id=com.shivatubewells.borewelldehradun&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="images/android.gif" alt="odex drilling dehradun"></a>
</div>
<section class="bg-white margin-bottom-sm" >
<section class="container-fluid">
<div class="float-right mt-2">
        <a id="" href="#bottomscroll" class="btn btn-danger1" role="button" data-toggle="popover" data-trigger="hover" data-content="Move To Bottom">
        <i class="fas fa-chevron-down"></i></a>
</div>
</section>
<!-- Scroll to top -->
<div class="pt-5 bg-color">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mx-auto text-center">
                <h1 class="txt-format">Welcome to Dehradun's most trusted borewell drilling site.</h1>
                <h2 class="txt-format h2-txt py-5">-SHIVA TUBEWELLS- <br><span><i class="fas fa-phone-volume text-success"></i></span> +91 8869081529</h2>
        
            </div>
        </div>
    </div>
</div>
<!-- <div class="half-circle mx-auto text-center">
<h2 class="txt-format py-5">-SHIVA TUBEWELLS- <br><span><i class="fas fa-phone-volume text-success"></i></span> +91 8869081529</h2>
</div> -->

<div class="container py-4" id="startchange">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-center my-3">ABOUT SERVICES</h3>
            <p style="font-size:16px;" class="text-justify"><br>We are renowned bore-well developer in the state. We
                deal with 5", 7" & 8" Odex drilling. We employ ODEX Percussion Down-The-Hole Hammer drilling method.
                It is an adaptation of the air-operated down-the-hole hammer. It uses a swing-out eccentric bit to
                ream the bottom of the casing. The percussion bit is a two-piece bit consisting of a concentric
                pilot bit behind which is an eccentric second bit that swings out to enlarge the hole diameter.
                Immediately above the eccentric bit is a drive sub that engages a special internal shouldered drive
                shoe on the bottom of the ODEX casing. The ODEX is thus pulled down by the drill stem as the hole is
                advanced. Cuttings blow up through the drive sub and casing annuls to a swivel conducting them to a
                sample collector or onto the ground. <br><br>Besides drilling, we are also authorized distributor of
                renowned submersible pumps in the state.We have been providing drilling services in the state for
                last 11 year. Services offered to commercial/non commercial and government projects. We have
                constructed at least one borewell in every part of the state. <br>Our team of highly experienced
                drillers and skilled workers discharge their duties with the aim to deliver satisfactory result. For
                any assistance or query regarding the bore-well drilling feel free to contact us any time. <br><b>To
                contact us please navigate to our<a href="contact-us.php"> contact page </a>.</b><br></p>
                <p style="font-size:16px;" class="text-justify">We own two DTH hydraulic drill with accessories truck
                    mounted for large dia LG compressor with capacity 1000/275 cfm/psi & 900/200 cfm/psi respectively.
                    <br>Please note that we also hire other rig machines in case of over booking. </p>
                </div>
            </div>
        </div>

        <div class="bg-color">
            <div class="container">
            <h3 class='text-center text-white py-5'>OUR SERVICES</h3>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="p-2 zoom">
                            <a href="#"><img src="images/slide2.jpg" alt="odex drilling dehradun uttrakhand" data-aos="zoom-in-right"
                                data-aos-duration="3000"></a>
                        </div>
                    </div><!-- col -->

                    <div class="col-lg-3 col-md-4">
                        <div class="p-2 zoom">
                            <a href="#"><img src="images/slide5.jpg" alt="water well drilling dehradun" data-aos="zoom-in-right"
                                data-aos-duration="3000"></a>
                        </div>
                    </div><!-- col -->

                    <div class="col-lg-3 col-md-4">
                        <div class="p-2 zoom">
                            <a href="#"><img src="images/slide6.jpg" alt="submersible pump dehradun"data-aos="zoom-in-left"
                                data-aos-duration="3000"></a>
                        </div>
                    </div><!-- col -->

                    <div class="col-lg-3 col-md-4">
                        <div class="p-2 zoom">
                            <a href="#"><img src="images/slide4.jpg" alt="5, 7, 8 inch ODEX drilling"data-aos="zoom-in-left"
                                data-aos-duration="3000"></a>
                        </div>
                    </div><!-- col -->
                </div><!-- row -->
            </div>
        </div>
    
        <div class="container py-5">
            <section class="row">
              <div class="col-sm-12">
                <div class="text-center txt-manage-center ">
                  <p>Shiva Tubewells.</p>
                  <h1>VALUES APPLIED TO EVERY PROJECT</h1>
              </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 txt-manage-center-1" data-aos="fade-up"
                data-aos-duration="3000">
            <img src="images/passion.png" alt="best tubewell contractor Dehradun">
            <h2>01</h2>
            <h1>SAFETY</h1>
            <p>For us, our drive for success is obvious in our work and everyday interactions with our clients and each other. We are passionate in our approach, and our goal is for every client to be 100% satisfied at project sign-off.</p>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 txt-manage-center-1" data-aos="fade-up"
                data-aos-duration="3000">
            <img src="images/01.png" alt="best borewell contractor Uttarakhand">
            <h2>02</h2>
            <h1>RELATIONSHIPS</h1>
            <p>Friendly, hardworking people. Loyal, repeat clients. Back and forth conversations that start with an idea and ends with a solution. Our team understands the importance of lasting relationships.
            </p>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 txt-manage-center-1"data-aos="fade-up"
                data-aos-duration="3000"> 
            <img src="images/reputation.png" alt="best shiva tubewells Services">
            <h2>03</h2>
            <h1>INTEGRITY</h1>
            <p>At Shiva Tubewells, we believe in doing what we say we will, the first time, every time! We will always be honest and accountable, that is in our character. Our reputation depends on it!</p>
        </div>
        <div class="col-sm-12">
            <div class="text-center txt-manage-center">
                <button type="button" class="btn btn-lg txt-btn" data-toggle="modal" data-target="#exampleModalCenter">
                    GET A FREE QUOTE
                </button>
            </div>
        </div>
    </section>
</div>

<section class="trusted-partner-sec" style="background-color:rgb(39,61,130);">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 txt-about trusted">
            <div class="txt-about-1"data-aos="fade-down"data-aos-duration="3000">
              <h6>Driven By Passon. United by Purpose.</h6>
              <h1>TRUSTED PARTNER</h1>
              <p>Through our unparalleled expertise in horizontal directional drilling, boring and underground construction, we can collaborate with our clients from the earliest stages of a project to meet their goals for quality, cost and timely delivery. </p>
              <button type="button" class="btn  btn-lg">
                  <a href="" class="text-decoration-none txt">About Shiva Tubewells</a>
              </button>
          </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 txt-about-center trusted">
          <div class="txt-about-1" data-aos="fade-up"
     data-aos-duration="3000">
              <h6>Superiority in Borewell drilling.</h6>
              <h1>TECHNOLOGY DRIVEN</h1>
              <p>We use cutting edge technology and latest equipment to provide best and superior quality to our customers.</p>
              <button type="button" class="btn  btn-lg">
                  <a href="" class="text-decoration-none txt-1 txt">About Shiva Tubewells</a>
              </button>
          </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 txt-about-right trusted">
          <div class="txt-about-1" data-aos="fade-down"data-aos-duration="3000">
              <h6>A penny saved is a penny earned.</h6>
              <h1>COST EFFECTIVE</h1>
              <p>We know the value of money and time thus we provide the best solutions which suit our customer’s budget. </p>
              <button type="button" class="btn  btn-lg">
                  <a href="" class="text-decoration-none txt">About Shiva Tubewells</a>
              </button>
          </div>
      </div>
  </div>
</div>
</section>

<section class="testimonials-sec">
    <div class="container">
        <div class="testimonial-bx">

            <div class="testimonials-dv">
                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108248202436083305644/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAS">
                                        Aman Batra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Amazing services. Got more than what I paid. Very professional and much recommended.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108248202436083305644/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAS">
                                    <img src="images/testimonials/photo.png" alt="amazing borewell services dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/113530444860659626471/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAZ">
                                        Vimal Devrari
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Very good work by Shiva tube well team!!  There behaviour, hard work and focus on there work is outstanding!!  U should contact them for drilling!!   </p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/113530444860659626471/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAZ">
                                    <img src="images/testimonials/photo1.png" alt="best odex drilling uttrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/101259021679572820440/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAg">
                                        Nand kishor Malhotra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>He give us very good service and helpful good  rate for boring</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/101259021679572820440/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAg">
                                    <img src="images/testimonials/photo2.png" alt="best water well drilling dehradun uttrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/117435212400259742287/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAn">
                                        Ashish Puri
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Shiva Tubewells are well versed in their services. Their drilling equipment and technology is very nice. Also, their team is very firm and quick at task. Good job..!!</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/117435212400259742287/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAn">
                                    <img src="images/testimonials/photo3.png" alt="best drilling equipment and technology dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108647230645944677346/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAu">
                                        Deepak Prajapati
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Awesome👏✊👍</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108647230645944677346/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARAu">
                                    <img src="images/testimonials/photo4.png" alt="best borewell services in dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/100334357424873365805/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA1">
                                        Sameer Arora
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Undoubtedly very fast service with affordable machinery parts. Their team is very supporting and helpful..!!</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/100334357424873365805/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA1">
                                    <img src="images/testimonials/photo5.png" alt="very fast borewell service with affordable machinery parts">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/116434053513280877021/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA8">
                                        Zoya Akhtar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>One of the best service providers in Rishikesh. They have a very dedicated team for their work.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/116434053513280877021/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARA8">
                                    <img src="images/testimonials/photo6.png" alt="best submersible pump service providers dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111267024750297042963/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBD">
                                        Kunal Mehta
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Good job done, you guys really helped us. Will recommend to others.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111267024750297042963/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBD">
                                    <img src="images/testimonials/photo7.png" alt="best borewell contractor Uttarakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/110964976779591695750/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBK">
                                        Heena Kalra
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>I was looking for a tubewell boring contractor in Haridwar and get in touch with Shiva Tubewells. They provided us fast service with all the additional machinery needed at a reasonable price. Recommended for any kind of drilling services..</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/110964976779591695750/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBK">
                                    <img src="images/testimonials/photo8.png" alt="fast borewell service with additional machinery dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/116370404157529843675/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBR">
                                        Pinkesh Tomar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Wonderful work and brilliant team of Shiva Tube wells</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/116370404157529843675/reviews?hl=en-IN&sa=X&ved=2ahUKEwilo6_pjIPlAhUIXisKHfcECKkQvfQBegQIARBR">
                                    <img src="images/testimonials/photo9.png" alt="brilliant team of borewell shiva tubewells dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/102911228534801445829/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAC">
                                        pavan kumar U
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Their service is really good. Can totally rely on the tubewell products here.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/102911228534801445829/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAC">
                                    <img src="images/testimonials/photo10.png" alt="fast odex drilling services dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/117232992997505591885/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAJ">
                                        SHIV GANGA TUBEWELLS
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>very good services</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/117232992997505591885/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAJ">
                                    <img src="images/testimonials/photo11.png" alt="very good 5, 7, 8 inch ODEX drilling services dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/109491887894467805369/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAQ">
                                        SANKARSHAN TRIPATHI
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Best for deliver long term as well as short term borewell construction.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/109491887894467805369/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAQ">
                                    <img src="images/testimonials/photo12.png" alt="short term borewell construction dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/112861573939838237585/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAX">
                                        Magin Ranjit
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>"Delivers what it promises"-  Got my 8 inch borewell developed over night at Hathibarkla, Dehradun 248001</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/112861573939838237585/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAX">
                                    <img src="images/testimonials/photo13.png" alt="best borewell contractor Uttarakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111365297219546722394/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAe">
                                        Shiva Tubewells
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Our motto is to deliver our best.</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111365297219546722394/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAe">
                                    <img src="images/testimonials/photo14.png" alt="tubewell contractor deliver best service Dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/112919184390109621646/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAl">
                                        Rajnish Nigam
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>Helpful</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/112919184390109621646/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAl">
                                    <img src="images/testimonials/photo15.png" alt="odex fast drilling dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/108550199442617753923/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAs">
                                        Ajay Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p>बहुत अच्छा काम करते है।</p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/108550199442617753923/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAs">
                                    <img src="images/testimonials/photo16.png" alt="tubewell contractor dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/115398594173184926157/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAz">
                                        Prateek Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/115398594173184926157/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARAz">
                                    <img src="images/testimonials/photo17.png" alt="5, 7, & 8 inch ODEX / DTH drilling dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/103579751596339231271/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA3">
                                        Utkarsh singh
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/103579751596339231271/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA3">
                                    <img src="images/testimonials/photo18.png" alt="water well drilling uttrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/100166325659664321249/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA6">
                                        Wotoki Awomi
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/100166325659664321249/reviews?hl=en-IN&sa=X&ved=2ahUKEwiruNHO_4PlAhWEXSsKHao1BlcQvfQBegQIARA6">
                                    <img src="images/testimonials/photo19.png" alt="fast submersible pump utrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/106404646044966935557/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAC">
                                        Ranjeet Kumar
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/106404646044966935557/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAC">
                                    <img src="images/testimonials/photo20.png" alt="best submersible services dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/111828459895964881075/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAF">
                                        Amardeep Gulati
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/111828459895964881075/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAF">
                                    <img src="images/testimonials/photo21.png" alt="fast tubewell contractor service uttrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/101872532016271317128/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAI">
                                        Mohan Sai Manthri
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/101872532016271317128/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAI">
                                    <img src="images/testimonials/photo22.png" alt="borewell contractor dehradun">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

                <div>
                    <div class="testimonials testimonials-archive">
                        <div class="testint">
                            <div class="text">
                                <div class="review-txt">
                                    <a href="https://www.google.com/maps/contrib/106891134047049238234/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAM">
                                        leeway handikraft
                                    </a>
                                </div>
                                <div class="review">
                                    <div class="star">
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p></p>
                            </div><!-- text -->
                        </div>
                        <div class="aboutClient d-flex">
                            <div class="ml-2 text-uppercase">
                                <a href="https://www.google.com/maps/contrib/106891134047049238234/reviews?hl=en-IN&sa=X&ved=2ahUKEwi2oKmOgITlAhVMQ48KHazhCxAQvfQBegQIARAM">
                                    <img src="images/testimonials/photo23.png" alt="best borewell contractor uttrakhand">
                                </a>
                            </div>
                        </div>
                    </div><!-- testimonials -->
                </div>

            </div><!-- testimonials -->

        </div><!-- testimonial-bx -->
    </div><!-- container -->
</section><!-- testimonials-sec -->
<section  class="forth-section text-center">
<div class="container py-5">
    <h3 class='text-center text-white pb-5'>THE RESULTS SPEAK FOR THEMSELVES.</h3>
    <div class="row">
    <div class='col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 py-3 mx-auto'>
        <div class="car-with m-auto">
        <!-- <img src="{{asset('images/results-car.png')}}" alt=""> -->
        </div>
    <div class="mx-3 text-white"  id="counter">
        <h1 class="text-info counter-value" data-count="800" ></h1>
        <h3>HIGHER</h3>
        <h6>AUTOALERT VALIDATED<br>
        SALES AT A HIGHER GROSS</h6>
    </div>
    </div>
    <div class='col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 text-white text-center mt-2'>
        <div class="border-circle-set mx-auto" id="counter">
        <h1 class="mt-3 text-info counter-value" data-count="1204"></h1>
        <h3>HIGHER</h3>
        </div>
    <div class="m-3">
       <h6>O2C BOUGHT<br>
        AT A HIGHER RATE</h6>
    </div>
    </div>
    <div class='col-sm-4 py-3 mt-2'>
        <div>
        <!-- <img src="{{asset('images/results-lift.png')}}" alt="" class='w-25'> -->
        </div>
    <div class="mx-3 text-white" id="counter">
        <h1 class="text-info counter-value" data-count="5000"></h1>
        <h3>INCREASE</h3>
        <h6>IN DEALER LOYALTY</h6>
    </div>
    </div>
</div>
</div>
</section>


<section class="container py-5">
    <h4 class="text-center">
        RELATIONSHIPS BUILT ON TRUST, AUTHENTICITY & GETTING THE JOB DONE RIGHT, EVERY TIME
    </h4>
    <div class="row">
        <!-- <div class="col-sm-2 col-sm-2"></div> -->
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
            <h6>DROP US A LINE</h6>
            <h3 class="text-mob">+91 8869081529</h3>
            <h3 class="text-mob">+91 9412938365</h3>
            <h4>-SHIVA TUBEWELLS-</h4>
            <p>Jhajra, Near Balaji Temple,Dehradun</p>
            <p><a href="mailto:info@shivatubewells.com" class="text-mob">info@shivatubewells.com</a></p>
            <span> <img src="images/trust-seal-removebg.png" class="trust-seal" alt="borewell guaranty quality work dehradun" /></span>
            <!-- <a href=""><img src="images/stwlogo.PNG" alt="" class="w-50"></a> -->
        </div>

        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
        <form name="freecontactform" method="post" action="freecontactformprocess.php" onsubmit="return get_action(this)">
                <input type="text" class="form-control form-txt" name="Full_Name" id="Full_Name" placeholder="Your Name" required="required" onkeyup="myFunction()">
                <input type="email" class="form-control form-txt" name="Email_Address" id="Email_Address" placeholder="Your Email" required="required">
                <input type="text" class="form-control form-txt Contact_Number"  name="Telephone_Number" id="phone_Number" placeholder="Mobile Number" required="required">
                <textarea name="Your_Message" id="Your_Message" rows="5" class="form-control form-txt" placeholder="Message" required="required"></textarea>
                <div class="g-recaptcha mt-3" id="rcaptcha"  data-sitekey="6LdC_cgUAAAAAJck4oLsxuYmX3uZQKLaXGZ8F7EM"></div>
                <span id="captcha" style="color:red"></span>
                <button type="submit" class="btn txt-btn btn-block submitbtn" value="Submit">SEND MESSAGE</button>
            </form>
        </div>
    </div>
</section>
<!-- Scroll to top -->
<section class="container-fluid">
<div class="float-right">
        <a id="" href="#" class="btn btn-danger2" role="button" aria-label="Scroll to top" data-toggle="popover" data-trigger="hover" data-content="Move To Top">
        <i class="fas fa-chevron-up"></i></a>
</div>
</section>
<!-- <div class="shadow"></div> -->
</section>
<div id="bottomscroll"></div>
<!-- Scroll to top -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!--         <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body text-center">
        <h1>Full screen Transparent Bootstrap Modal</h1>
        <p>FEEL FRREE TO GET YOUR MODAL CODE HERE FOLKS.</p>
        <a class="pre-order-btn" href="#">GET THE MODAL CODE</a>
      </div>
      <div class="modal-footer">
        <!--         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>


<script>
    $('.carousel').carousel({
        interval: 2000,
        pause: "false"
    });
    var $item = $('.carousel-item');
    // var $wHeight = $(window).height();
    $item.eq(0).addClass('active');
    $item.height($wHeight);
    $item.addClass('full-screen');

    $('.carousel img').each(function() {
        var $src = $(this).attr('src');
        var $color = $(this).attr('data-color');
        $(this).parent().css({
            'background-image': 'url(' + $src + ')',
            'background-color': $color
        });
        $(this).remove();
    });
</script>

<!-- counter -->
<script>
var a = 0;
$(window).scroll(function() {

  var oTop = $('#counter').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 3000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});
</script>
