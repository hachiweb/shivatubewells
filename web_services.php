<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
require_once 'dbconnect.php';
// function require_auth() {
//     $AUTH_USER = 'stwells';
//     $AUTH_PASS = 'stw@#123';
//     header("Content-Type:application/json");
//     header('Cache-Control: no-cache, must-revalidate, max-age=0');
//     $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
//     $is_not_authenticated = (
//         !$has_supplied_credentials ||
//         $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
//         $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
//     );
//     if ($is_not_authenticated) {
//         header('HTTP/1.1 401 Authorization Required');
//         header('WWW-Authenticate: Basic realm="Access denied"');
//         print_r('Access denied');
//         die();
//     }
    // else{
        $endpoint = $_GET['endpoint'];
        
        $db = new DB();
        if(isset($_POST) && !empty($_POST)){
            $name = $_POST['name'];
            $email  = $_POST['email'];
            $phone  = $_POST['number'];
            $illegal = "#$%^&*()+=-[]';,./{}|:<>?~";
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === TRUE) {
                $msg = "{$email} is invalid";
                $response['status'] = '403';
                $response['msg'] =  $msg;
                echo json_encode($response);
                exit();
            }
            if (!filter_var($phone, FILTER_VALIDATE_INT) === TRUE)
            {   
                $msg = "{$phone} is invalid";
                $response['status'] = '403';
                $response['msg'] =  $msg;
                echo json_encode($response);
                exit();
            }
            
        }
        
        $isGet = 0;
        if(!empty($endpoint)) {
            if($endpoint == 'gettext'){
                $sql  = "SELECT * FROM `content` WHERE `type` = 0";
            } 
            else if($endpoint == 'getimages'){
                $sql  = "SELECT * FROM `content` WHERE `type` = 1";     
            }
            else if($endpoint == 'submit-contact'){
                $message  = $_POST['message'];
                $mail = new PHPMailer;
                // $mail->isSMTP();
                $mail->Host = 'hachiweb.com';
                $mail->Port = 587;
                $mail->SMTPAuth = true;
                $mail->Username = 'care@hachiweb.com';
                $mail->Password = 'Vw4&2tw4';
                $mail->setFrom('care@hachiweb.com', 'Shiva Tubewells');
                $mail->addAddress('infoshivatubewells@gmail.com');
                // Add cc or bcc 
                $mail->addCC('mohansaimanthri03@gmail.com');
                $mail->addBCC('test@hachiweb.com');
                $sql="INSERT INTO `contact`(`name`,`email`,`contact`,`message`)
                VALUES ('$name','$email','$phone','$message')";
                $isGet = 1;
                // if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
                    $mail->Subject = 'Shiva Tubewells Contact us';
                    $mail->isHTML(false);
                    $mail->Body = "
                    Name: {$name} 
                    Email: {$email} 
                    Contact: {$phone} 
                    Message: {$message}";
                    if ($mail->send()) {
                        $response['status'] = 200;
                        $response['message'] = 'Thank you for contacting us. We will get back to you shortly';
                        header('Content-Type: application/json');
                        header("HTTP/1.1 200 OK");
                        // echo json_encode($response);
                    }
                    else {
                        $response['status'] = 400;
                        $response['message'] = $mail->ErrorInfo;
                        header('Content-Type: application/json');
                        header("HTTP/1.1 400 Bad request");
                        echo json_encode($response);
                        exit();
                    }
            }
            else if($endpoint == 'submit-quote'){
                $site  = $_POST['site'];
                $bore_dia  = $_POST['bore_dia'];
                $mail = new PHPMailer;
                // $mail->isSMTP();
                $mail->Host = 'hachiweb.com';
                $mail->Port = 587;
                $mail->SMTPAuth = true;
                $mail->Username = 'care@hachiweb.com';
                $mail->Password = 'Vw4&2tw4';
                $mail->setFrom('care@hachiweb.com', 'Shiva Tubewells');
                $mail->addAddress('info@shivatubewells.com', 'Shiva Tubewells');
                // Add cc or bcc 
                $mail->addCC('mohansaimanthri03@gmail.com');
                $mail->addBCC('infoshivatubewells@gmail.com');
                $sql="INSERT INTO `quotes`(`name`,`email`,`contact`,`site`,`bore_dia`) 
                VALUES ('$name','$email','$phone','$site','$bore_dia')";
                if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
                    $mail->Subject = 'Shiva Tubewells Quotation Request';
                    $mail->isHTML(false);
                    $mail->Body = "
                    Name: {$name}
                    Email: {$email}
                    Contact: {$phone}
                    Site: {$site}
                    Bore Dia: {$bore_dia}";
                    if (!$mail->send()) {
                        $msg = 'Sorry, something went wrong. Please try again later.';
                    } else {
                        $msg = 'Thank You. We have received your message. Our customer care representative will be contacting you shortly.';
                    }
                } else {
                    $msg = '';
                }
            
                $isGet = 1;
            }
            else {
                echo 'Invalid end-point';
            }
            if($isGet == 0){
                $result = $db->executeQuery($sql);
                if($result){
                    $response['status'] = 'Success';
                }
                // while($response1 = mysqli_fetch_assoc($result)){
                //     $response['data'][] = $response1;
                // } 
                $json_array = array();  
                while($row = mysqli_fetch_assoc($result))  
                {  
                        $json_array[] = $row;  
                }  
                // echo '<pre>';  
                // print_r(json_encode($json_array));  
                // echo '</pre>';
                // echo json_encode($json_array);
                // print_r(json_encode($response));
            }
            else {
                $result = $db->executeQuery($sql);
                if($result){
                    $response['status'] = 200;
                    $response['message'] = 'Success';
                    header('Content-Type: application/json');
                    header("HTTP/1.1 200 OK");
                    echo json_encode($response);
                }
                else {
                    $response['status'] = 400;
                    $response['message'] = 'Bad request';
                    header('Content-Type: application/json');
                    header("HTTP/1.1 400 Bad request");
                    echo json_encode($response);
                }
            }
        }
//     }
// }
// require_auth();
   
   ?>